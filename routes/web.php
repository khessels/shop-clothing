<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use App\Item;
use App\Shop;
use App\User;

Route::get('/', function () {
    return view('index');
});
Route::get('/cart', function () {
    return view('cart');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/product-detail', function () {
    return view('product-detail');
});
Route::get('/product/sell', function () {
    if (Auth::check()) {
        $userId = Auth::id();
        $shop = App\Shop::where('owner_id',$userId)->get();
        return view('product-sell', ['currency'=>'USD']);
    }else{
        return redirect()->route('login');
    }
});
Route::get('/products', function () {
    $items = App\Item::where('active','YES')->orderBy('id')->get();
    return view('products');
});
Route::get('/faq/selling-101', function () {
	return view('faq-selling-101');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/checkout', function () {
	return view('checkout');
});
Route::get('/item/create', function () {
	return view('item-create');
});
Route::get('/item/management', function () {
	return view('item-management');
});
Route::get('/media/upload', function () {
	return view('media-upload');
});
Route::post('/media/upload', 'UploadController@uploadMedia');

Auth::routes();
Route::get('/home', 'HomeController@index');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
