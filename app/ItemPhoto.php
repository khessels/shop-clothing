<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPhoto extends Model
{
    protected $fillable = ['item_id', 'filename'];
    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
