@extends('layouts.layout-generic')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('banner')
    @include('components.banner')
@endsection


@section('footer-bar')
    @include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
    @include('components.bars.copyright-bar')
@endsection
@section('content')
    <h3>Selling 101</h3>
    <h4>tldr;</h4>
    <p>&nbsp;</p>
    <p>It's quite simple really. there are 3 steps involved and we assume that you have been registered and verified.</p>
    <p>1. Make photos and videos of the item(s) you want to sell. <br>
        &nbsp;Tip: Try to make at least 4 photo's and a video of max 10 seconds of each product you wish to sell.<br>
    </p>
    <p>2. Create the item you want to sell. <br>
        &nbsp;At this stage you will add all the text, price, quantities etc.. and assign the media(images) to the item you want to sell.<br>
    </p>
    <p>3. Item management<br>
        &nbsp; This is the place where you actually enable an item to be sold, or you disable it when you are sold out.<br>
        &nbsp; <strong>** WARNING **</strong> please care full. We know you like to sell, however when you sell something that you do not have in stock any more, <br>
        &nbsp; and not be able to ship within 3 days, the customer can void the order and he will be refunded.<br>
        &nbsp; On top of that there will be a penalty of 15% of the price which will be automatically deducted from your sales.
    </p>
@endsection