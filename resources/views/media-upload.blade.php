@extends('layouts.layout-generic')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('dropZone')
    @include('components.dropZone')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('banner')
    @include('components.banner')
@endsection


@section('footer-bar')
    @include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
    @include('components.bars.copyright-bar')
@endsection
@section('content')
    <h3>Upload media:</h3>
    <h4>Photos, Videos, etc..</h4>
    <p><strong>Note: </strong> Video's max 10 seconds, images minimal 320 x 240 resolution</p>
    <form action="/media/upload" class="dropzone"></form>
@endsection
@section('dropZone-js')
    @include('components.dropZone-js')
@endsection