@extends('layouts.product-detail')
@extends('layouts.products')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('banner')
    @include('components.banner')
@endsection
@section('slider-related-products')
    @include('components.sliders.slider-related-products')
@endsection


@section('footer-bar')
	@include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
	@include('components.bars.copyright-bar')
@endsection

@section('product-detail-javascript')
    @include('components.product-detail-javascript')
@endsection

@section('fancy-box-header')
    @include('components.box.fancy-box-header')
@endsection