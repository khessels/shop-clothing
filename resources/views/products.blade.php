@extends('layouts.products')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('banner')
    @include('components.banner')
@endsection

@section('product-list')
    @include('components.list.product-grid-3')
@endsection

@section('footer-bar')
    @include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
    @include('components.bars.copyright-bar')
@endsection

