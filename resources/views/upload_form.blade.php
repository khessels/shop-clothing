@extends('layouts.layout-generic')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('banner')
    @include('components.banner')
@endsection


@section('footer-bar')
    @include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
    @include('components.bars.copyright-bar')
@endsection
@section('message')
    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection
@section('content')
<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    Product name:
    <br />
    <input type="text" name="name" />
    <br /><br />
    Product photos (can attach more than one):
    <br />
    <input type="file" name="photos[]" multiple />
    <br /><br />
    <input type="submit" value="Upload" />
</form>
@endsection