@extends('layouts.product-sell')

@section('head-generic')
	@include('components.head-generic')
@endsection
@section('top-bar')
	@include('components.bars.top-bar')
@endsection
@section('nav-bar')
	@include('components.bars.nav-bar')
@endsection
@section('banner')
	@include('components.banner')
@endsection

@section('product-sell-category')
	@include('components.product.category')
@endsection
@section('sell-item')
	@include('components.product.clothing.sell-woman')
@endsection

@section('footer-bar')
	@include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
	@include('components.bars.copyright-bar')
@endsection

