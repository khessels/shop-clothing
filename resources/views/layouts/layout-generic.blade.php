<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @yield('head-generic')
    @yield('slider-flex-head')
    @yield('dropZone')
</head>
<body>
@yield('top-bar')

<div id="wrapper" class="container">
    @yield('nav-bar')
    @yield('slider-flex')
    @yield('message')

    <section class="main-content">
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="col-md-3">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </section>
    @yield('our-clients')
    @yield('footer-bar')
    @yield('copyright')
</div>
<script src="/themes/js/common.js"></script>
@yield('slider-flex-javascript')
@yield('dropZone-js')
</body>
</html>