<!DOCTYPE html>
<html lang="en">
<head>
    @yield('head-generic')
    <script src="/js/dropzone.js"></script>
</head>
<body>
    @yield('top-bar')
    <div id="wrapper" class="container">
        @yield('nav-bar')
        @yield('banner')

        <section class="main-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="span9">
                        @yield('product-sell-category')
                        @yield('sell-item')
                    </div>

                </div>
                <div class="col-md-9">
                    <form action="/file"
                          class="dropzone"
                          id="my-awesome-dropzone">
                        <input type="file" name="file" />
                        <input type="submit" value="Upload">
                    </form>
                </div>
            </div>
        </section>
        @yield('footer-bar')
        @yield('copyright-bar')
    </div>
<script src="/themes/js/common.js"></script>
</body>
</html>