<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @yield('head-generic')
    @yield('slider-flex-head')
</head>
<body>
@yield('top-bar')

<div id="wrapper" class="container">
    @yield('nav-bar')
    @yield('slider-flex')
    @yield('message')

    <section class="main-content">
        <div class="row">
            <div class="span12">
                <div class="row">
                    @yield('slider-feature-products')
                </div>
                <br/>
                <div class="row">
                    @yield('slider-latest-products')

                </div>
                @yield('feature-box')
            </div>
        </div>
    </section>
    @yield('our-clients')
    @yield('footer-bar')
    @yield('copyright')
</div>
<script src="/themes/js/common.js"></script>
@yield('slider-flex-javascript')
</body>
</html>