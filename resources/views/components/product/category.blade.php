@section('product-sell-category')
    <label>Category</label>
    <select id="category">
        <option selected="selected">CLOTHING</option>
    </select>
    <label>Sub Category</label>
    <select id="sub_category">
        <option selected="selected">WOMAN</option>
        <option>MAN</option><option>YOUNG ADULT</option>
        <option>CHILDREN</option>
        <option>ACCESSORIES - HANDBAGS</option>
    </select>
@endsection