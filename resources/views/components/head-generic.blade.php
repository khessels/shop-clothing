@section('head-generic')
<meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <!--[if ie]>
        <meta content='IE=8' http-equiv='X-UA-Compatible'/>
    <![endif]-->
    <!-- bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/themes/css/bootstrappage.css" rel="stylesheet"/>
    <link href="/themes/css/main.css" rel="stylesheet"/>

    <script src="/themes/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/themes/js/superfish.js"></script>
    <script src="/themes/js/jquery.scrolltotop.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>

<script>
window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
]) !!};
</script>
@endsection