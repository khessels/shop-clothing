@section('top-bar')
<div id="top-bar" class="container">
    <div class="row">
        <div class="span4">
            <form method="POST" class="search_form">
                <input type="text" class="input-block-level search-query" Placeholder="eg. T-sirt">
            </form>
        </div>
        <div class="span8">
            <div class="account pull-right">
                <ul class="user-menu">
                    @if (Auth::guest())
                        <li><a href="/product/sell">Start selling</a></li>
                        <li><a href="/cart">Your Cart</a></li>
                        <li><a href="/checkout">Checkout</a></li>
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register">Register</a></li>
                    @else
                        <li><a href="/faq/selling-101">Selling 101</a></li>
                        <li><a href="/media/upload">Upload photos</a></li>
                        <li><a href="/item/create">Create items</a></li>
                        <li><a href="/item/management">Item management</a></li>
                        <li><a href="/cart">Your Cart</a></li>
                        <li><a href="/checkout">Checkout</a></li>
                        <li><a href="#">My Account</a></li>
                        <li><a href="{{ url('/logout') }}"> logout </a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection