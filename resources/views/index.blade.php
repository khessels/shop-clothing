@extends('layouts.layout')

@section('head-generic')
    @include('components.head-generic')
@endsection
@section('top-bar')
    @include('components.bars.top-bar')
@endsection
@section('nav-bar')
    @include('components.bars.nav-bar')
@endsection
@section('message')
    @include('components.message')
@endsection
@section('slider-feature-products')
    @include('components.sliders.slider-feature-products')
@endsection

@section('slider-latest-products')
    @include('components.sliders.slider-latest-products')
@endsection
@section('feature-box')
    @include('components.feature-box')
@endsection
@section('our-clients')
    @include('components.our-clients')
@endsection
@section('footer-bar')
    @include('components.bars.footer-bar')
@endsection
@section('copyright-bar')
    @include('components.bars.copyright-bar')
@endsection

@section('slider-flex-head')
    @include('components.sliders.slider-flex-head')
@endsection
@section('slider-flex')
    @include('components.sliders.slider-flex')
@endsection
@section('slider-flex-javascript')
    @include('components.sliders.slider-flex-javascript')
@endsection











